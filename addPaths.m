function addPaths()
warning('off','all')
currentPath = path;
d = which('addPaths');
currentDir = pwd;
[pathstr, name, ext] = fileparts(d); 
restoredefaultpath;
cd(pathstr)
%update
%only for compiled version
try
system('git config merge.renameLimit 999999');
system('git fetch --all');
system('git reset --hard origin/master');
%system('git config --unset merge.renameLimit');
system('git pull');
catch
    disp('Install git to get updates')
end

matlabV = [100, 1] * sscanf(version, '%d.%d', 2); %getmatlab version

if matlabV<805
GUIinstall = fullfile(pathstr, 'MatlabExt', 'GUILayout-v1p17', 'install.m');
if(~exist(GUIinstall)~=2)
GUIlayout = fullfile(pathstr, 'MatlabExt', 'GUILayout-v1p17');
cd(GUIlayout)
install
end
else
  addpath(genpath(fullfile(pathstr, 'MatlabExt','GUILayout221')));  
end
addpath(genpath(fullfile(pathstr, 'main')));
addpath(fullfile(pathstr, 'fieldtrip'));
addpath(fullfile(pathstr, 'MatlabExt'));
if matlabV<805
addpath(fullfile(pathstr, 'eeglab'));
addpath(fullfile(pathstr, 'eeglab', 'plugins'));
addpath(genpath(fullfile(pathstr, 'eeglab', 'plugins', 'erplab_4.0.3.1')));
%addpath(genpath(fullfile(pathstr, 'eeglab', 'external')));
%addpath(genpath(fullfile(pathstr, 'eeglab', 'functions')));
%addpath(genpath(fullfile(pathstr, 'eeglab', 'sample_data')));
%addpath(genpath(fullfile(pathstr, 'eeglab', 'sample_locs')));
%eeglab 2
else
addpath(fullfile(pathstr, 'eeglab'));
addpath(fullfile(pathstr, 'eeglab', 'plugins'));
addpath(genpath(fullfile(pathstr, 'eeglab', 'plugins', 'erplab_4.0.3.1')));
%addpath(genpath(fullfile(pathstr, 'eeglab13_5_4b', 'external')));
%addpath(genpath(fullfile(pathstr, 'eeglab13_5_4b', 'functions')));
%addpath(genpath(fullfile(pathstr, 'eeglab13_5_4b', 'sample_data')));
%addpath(genpath(fullfile(pathstr, 'eeglab13_5_4b', 'sample_locs')));
end

addpath(fullfile(pathstr, 'binica'));
addpath(fullfile(pathstr, 'helpers'));
addpath(fullfile(pathstr, 'ERPWAVELABv1.2'));
ft_defaults
eegBatch();
eeglab
cd(currentDir)

end
%ftp://sccn.ucsd.edu/pub/daily/
%ftp://ftp.fcdonders.nl/pub/fieldtrip/