function generateExcelResult(FACT, chanlocs, tim, fre, filename, datasetname)
% Function generating Excel/worksheet-file summarizing the decomposition
% results
%
% Written by Morten M�rup
% 
% Usage:
%       generateExcelResult(Fact, chanlocs, tim, fre, datasetname)
%
% Input:
%       FACT         Cell array of decomposition result
%       chanlocs     The location of the channels as defined by EEGLAB
%       tim          The location of each time sample in ms. of the time dimension
%       fre          The location of each frequency sample in ms. of the
%                    frequency dimension.
%       datasetname  Cell array containing the name of each dataset
%       filename     filename including path in which to save the
%                    decomposition summary


% Generate Excel cells of FACT{1} and FACT{2}
summary=cell{}
if length(FACT)==2 & nargin==6  % 2-way Ch x Fr x Tim decomposition result
    summary{1,2}='Channel';
    summary{1,3}='Dataset';
    summary{1,4}='Time';
    summary{1,5}='Frequency';
    summary{1,6}='TF-Value';

    H=FACT{2}';
    Q=[1 length(fre) length(tim)  size(FACT{2},1)/(length(fre)*length(tim))];
    q=0;
    for k=1:size(FACT{1},2);        
        q=q+(k-1)*length(datasetname);
        % Generate array of dataset x frequency x time;
        Ht{k}=squeeze(unmatrizicing(H(k,:),1,Q));
        Ht{k}=permute(Ht{k},[3, 1, 2]);
        
        % Generate cells for excel sheet
        summary{q+k+1,1}=['Component ' num2str(k)];
        [y,i]=max(FACT{1}(:,k));
        summary{q+k+1,2}=chanlocs(i).labels;
        for t=1:size(Ht{k},1)
           summary{k+1,2}=datasetname(t);
           tH=squeeze(Ht{k}(t,:,:));
           a=max(tH(:));
           a=a(1);
           [I,J]=find(tH==a);
           summary{q+t+1,4}=tim(J(1));
           summary{q+t+1,5}=fre(I(1));
           summary{q+t+1,6}=a;
        end
    end
else     % 2-way Ch x Fr or 3-way  Ch x Fr x Tim decomposition result
    summary{1,2}='Channel';
    summary{1,3}='Time';
    summary{1,4}='Frequency';
    summary{1,5}='TF-Value';

    for k=1:size(FACT{2},2) 
        H=squeeze(unmatrizicing(FACT{2}(:,k)',1,[1 length(fre) length(tim)]));
        [I,J]=find(H==max(H(:)));
        [Y,C]=max(FACT{1}(:,k));
        summary{k+1,1}=['Component ' num2str(k)];
        summary{k+1,2}=chanlocs(C).labels;
        summary{k+1,3}=tim(J(1));
        summary{k+1,4}=fre(I(1));
        summary{k+1,5}=H(I(1),J(1));
    end
end

% Generate Excel cells of FACT{3}
if length(FACT)>2
   for k=1:size(FACT{3},1)
       summary{1,6+k}=datasetname{k};
       for j=1:size(FACT{3},2)
           summary{1+j,6+k}=FACT{3}(k,j);
       end
   end
end

% Write Excel file
xlswrite(filename,summary);
