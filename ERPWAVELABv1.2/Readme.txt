Welcome to ERPWAVELAB

To start the program type:
ERPWAVELAB.m in the MATLAB prompth

To learn how to use ERPWAVELAB go to the Help menu and select Tutorial.

For more information on ERPWAVELAB and updates go to www.erpwavelab.org

To contact ERPWAVELAB email: erpwavelab@erpwavelab.org

Before using ERPWAVELAB read the ERPWAVELABlicense document provided with ERPWAVELAB.
Notice, no warranty for the program is given.

NB: You should not combine erpwavelab datasets from different versions of the ERPWAVELAB toolbox.