for ifile = 1:numel(fileList)
EE = erpwavelab_bt(fileList{ifile});
[path,filename,ext]=fileparts(fileList{ifile});
fiedtripTFR = EE.convertErpwavelabToFieldtripTimeFreq();
EE.setLayout('64EEG');

cfg=[];
    cfg.latency     = [100 200]; %can be 'all', 'prestim', 'poststim' ,[beg end]
    cfg.avgovertime = 'no'; % 'yes' or 'no' (default = 'no')
    %cfg.nanmean     = % 'yes' or 'no' (default = 'no')
 
    cfg.frequency   = [38 42];% can be 'all'  cfg.frequency   = [beg end]
    cfg.avgoverfreq = 'no'; %'yes' or 'no' (default = 'no')
   % cfg.nanmean     = string, can be 'yes' or 'no' (default = 'no')
 [fiedtripTFR2] = ft_selectdata(cfg, fiedtripTFR);
 
 zlim = [0 0.5]; % [beg end]  'maxmin', 'maxabs', 'zeromax', 'minzero', or [zmin zmax] 
 xlim = 'maxmin' % [beg end] or if set during select 'maxmin'
%figureH = EE.plotTopoplotTFR(fiedtripTFR2,xlim,zlim, 'absoulteRR', 'no')
cfg.zlim = zlim;
cfg.xlim =xlim;
cfg.maskstyle    = 'saturation';
cfg.layout       =  EE.helperVariables.layout;
cfg.marker       = 'off';
cfg.channel = ft_channelselection({'all','-CP6','-C6'}, fiedtripTFR2.label);
			% cfg.style
            figureH=figure;
            ft_topoplotTFR(cfg, fiedtripTFR2);
        
%print(figureH, filename, '-dpng');
%close(figureH);  
end



%makeLayoutFromSet(this,setFile) or


