

%channels= [1,2,3,5,6,7,14,15,16,23,24,25,28,29,30,39,40,41];
channels= [6, 15, 40];

alldata = 0;% 1 if all data no periods
periodFrom = [-500, 0]; %ms
periodTo = [0, 500];% ms
powerMethod = 'EEGlabLogSqrt' % 'EEGlabNoLog'; % 'EEGlabLog', 'FFTpower' EEGlabLogSqrt
ifERP = 1; % average to erp? if yes =1
freqRange = [1 150];

% directory to save to
addon = 'powerNoLogSqrt_FzCzFcz';


disp('#### Start analysis #### ');
errDat = '';
ierrDat = 1;
for iFile = 1:numel(fileList)
    sprintf('processing file %d from %d', iFile, numel(fileList))
    ALLEEG=[];
    EEG=[];
    CURRENTSET=[];
    [ALLEEG, CURRENTSET,  EEG, errDatset]=EEGlabOpenFiles(fileList{iFile},'EEGlabON', 'OFF');
    if ~strcmpi(errDatset, '0000')
        errDat{ierrDat} = errDatset; ierrDat=ierrDat+1;
    else
        
        
        powerModel = bt_power();
        powerModel.setEEGlabData(ALLEEG,EEG,CURRENTSET);
        
        [path,name,ext]= fileparts(fileList{iFile});
        powerModel.SaveDir  = fullfile(path, addon);
        for ip = 1:numel(periodTo)
            
            posi = round( (periodFrom(ip)/1000-EEG.xmin)*EEG.srate )+1;
            posf = min(round( (periodTo(ip)/1000-EEG.xmin)*EEG.srate )+1, EEG.pnts );
            pointrange = posi:posf;
            
    
            if ifERP == 1 && alldata ==0
                SIGTMP = EEG.data(channels,pointrange,:);
                SIGTMP = mean(SIGTMP,3);
                totsiz = length( pointrange);
            elseif alldata == 0
                SIGTMP = EEG.data(channels,pointrange,:);
                totsiz = length( pointrange);
            else
                SIGTMP = EEG.data(channels,:,:);
                totsiz = EEG.pnts;
                
            end
            
            
            switch powerMethod
                case 'EEGlabNoLog'
                    [spectra, freqs] = powerModel.EEGlabPSD(SIGTMP, EEG.srate,totsiz,freqRange,EEG.srate/256);
                case 'EEGlabLog'
                    [spectra, freqs] = powerModel.EEGlabLogPower(SIGTMP, EEG.srate,totsiz, freqRange,EEG.srate/256);
                case 'EEGlabLogSqrt'
                    [spectra, freqs] = powerModel.EEGlabPSDsqrt(SIGTMP, EEG.srate,totsiz,freqRange,EEG.srate/256);
                case 'FFTpower'
                    [spectra, freqs] = powerModel.makepower(SIGTMP, EEG.srate);
            end
            POWER(ip).name = name;
            POWER(ip).pointrange = [num2str(periodFrom(ip)) ':'  num2str(periodTo(ip))];
            POWER(ip).spectra = spectra;
            POWER(ip).freqs = freqs;
            POWER(ip).channels = EEG.chanlocs(channels);
            
        end
        
        if ~isdir( powerModel.SaveDir)
            mkdir( powerModel.SaveDir)
        end
        filename = fullfile( powerModel.SaveDir,[name '.mat']);
        save(filename,'POWER');
        
    end
 
end
   disp('#####Klaidos:#####');
if ~isempty(errDat)
    emptyCells = cellfun(@isempty,errDat);
    disp(char(['Datset:' errDat]));
end
clear all
disp('#### END #### ');
