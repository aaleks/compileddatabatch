function makeVisible(savedir, redraw)
	dataDir = savedir
	name = ['*'];
	ext = '.fig';
	shouldIcrawlFolders=1; % setup if files in folders
	% filter files to work with
	fileList=GetFilesWithExt('dataDir',dataDir,'dataFileName',name,'extension',ext,'crawlFolders', shouldIcrawlFolders);
	for ifile=1:numel(fileList)
		[filePath,fileName, fileExt]=fileparts(fileList{ifile});
		h1 = openfig(fileList{ifile})
		set(h1,'Visible','on')
		savefig(fullfile(filePath, fileName))
		if redraw == 1
		saveas(h1,fullfile(filePath,fileName),'png')
		end
		close(h1);
	end
end
