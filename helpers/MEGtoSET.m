%% to make EEGlab set from meg data

%% from brainstorm data files - adjust parameters mark and press F9
%fileList must be saved in workspace
% EEGlab sets will be saved to current matlab  folder

SubjectName = '1';
data = meg_bt();
data.setFromBrainstormFilesDefault(fileList, SubjectName)

% if channels are not default/306
%chanlocs loaded to workspace
SubjectName = '1';
data = meg_bt();
data.setFromBrainstormFiles(fileList, chanlocs, SubjectName)




