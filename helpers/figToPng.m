function figToPng(savedir, makevisible)
	% savedir = sprintf('/home/aleks/Documents/Titr-profiles/Titr-30-Jun-2020-ITPC-absolute-0-150');
	dataDir = savedir
	name = ['*'];
	ext = '.fig';
	shouldIcrawlFolders=1; % setup if files in folders
	% filter files to work with
	fileList=GetFilesWithExt('dataDir',dataDir,'dataFileName',name,'extension',ext,'crawlFolders', shouldIcrawlFolders);
	for ifile=1:numel(fileList)
		[filePath,fileName, fileExt]=fileparts(fileList{ifile});
		h1 = openfig(fileList{ifile})
		if strcmpi(makevisible, 'on')
			set(h1,'Visible','on')
		end
		saveas(h1,fullfile(filePath,fileName),'png')
		close(h1);
	end
end
