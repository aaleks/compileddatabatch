maximumas1=[];
grandAverageTopo = [];
for imainList = 1:numel(mainList)
DT = [];
fileList = mainList{imainList};
for ifile = 1:numel(fileList)
    
    
    [path,name,ext] = fileparts(fileList{ifile});
    bb = erpwavelab_bt(fileList{ifile});
    bb.setLayout('64EEG');
    data = bb.convertErpwavelabToFieldtripTimeFreq();
    cfg= [];
    channelsTopo = {'all', '-PO3','-M2','-M1'};
    cfg.channel = ft_channelselection(channelsTopo,data.label);
    cfg.avgoverchan = 'no';
    cfg.frequency  = [38 42];
    cfg.avgoverfreq = 'no';
    cfg.latency = [200, 500];
    cfg.avgovertime = 'no';
    [dataTopo] = ft_selectdata(cfg,data);
 
    
    DTTopo{ifile} = dataTopo;

end

grandAverageTopo{imainList} = ft_freqgrandaverage([],DTTopo{:});

end

for igrand= 1:numel(grandAverageTopo)
maximumas1(igrand) = max(max(max(grandAverageTopo{igrand}.powspctrm)));
end
maximumas = max(maximumas1);


for igrand = 1:numel(grandAverageTopo)
    cfg = [];
    cfg.colorbar  ='yes';
    cfg.showlabels = 'no';
    cfg.showoutline ='yes';
    cfg.interactive = 'no';
    cfg.linewidth     = 1;
    %cfg.zlim = [0 0.6];
    cfg.xlim =[200 500];
    cfg.maskstyle    = 'saturation';
    cfg.layout       =  bb.helperVariables.layout;
    cfg.marker       = 'off';
    figureH=  figure;
    ft_topoplotTFR(cfg, grandAverageTopo{igrand});
    [path,name,ext]=fileparts(mainList{igrand}{1});
    saveDir = fullfile(path,'grandAverageTOpo3');
    if ~isdir(saveDir)
        mkdir(saveDir);
    end
    filename = fullfile(saveDir,[name '_avg']);
    savefig(figureH,filename);
    print(figureH, filename, '-dpng');
    close(figureH);
    grandAvg = grandAverageTopo{igrand};
    save(fullfile(saveDir,'grandAverageFTTopo'),'grandAvg');
end


