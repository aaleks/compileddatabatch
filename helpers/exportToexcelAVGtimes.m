
timesfrom = [0,0.2];
timesTo = [0.1,0.5];


for iFileList =1:numel(fileList)
    load(fileList{iFileList})
    [path, nameExcel, ext] = fileparts(fileList{iFileList});
    writer = bt_excell_csvExport('xls', nameExcel, path);
    
    for ichan =1:numel(data.calculation(1).chanlocs)
        
        c=cellstr('times/names');
        for it=1:numel(timesfrom)
            n{it,1}=( [num2str(timesfrom(it)) '_' num2str(timesTo(it))] );
        end
        
        excell = [c; n];
        
        
        for ii = 1:numel(data.calculation)
            for tt = 1:numel(timesfrom)
                
                [path, name, ext] =  fileparts(data.matFileList{ii});
                [idxfrom,closestfrom] = FindTrigerTime(data.calculation(ii).tim,timesfrom(tt));
                [idxto,closestto] = FindTrigerTime(data.calculation(ii).tim,timesTo(tt));
                dataRow(tt,1) = num2cell(mean(squeeze(data.calculation(ii).WT(ichan,1,idxfrom:idxto))));
                
            end
            
            c=cellstr(name);
            n=(dataRow);
            dataR=[c; n];
            excell = [excell, dataR];

            channel = data.calculation(ii).chanlocs(ichan).labels;
    
        end
        writer.exportToXls(excell,ichan,channel,'A1')
    end
    
    
    
    
end%ifilelist