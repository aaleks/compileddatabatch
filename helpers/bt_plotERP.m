% average and save
% plot average
% possibility to plot all





% select data
cfg = [];
%desired ={'all', '-M1', '-M2'};
desired = {'gui'};
%desired = load('desiredChannels.mat');
cfg.channel   = ft_channelselection(desired,data.label);
cfg.avgoverchan = 'no';

% cfg.channelcmb     = FT_CHANNELCOMBINATION();
%cfg.avgoverchancmb = 'yes';

cfg.latency = [0.2,0.4];
cfg.avgovertime  = 'no';
%  cfg.frequency   = [18, 45];
%   cfg.avgoverfreq = 'no';

data = ft_selectdata(cfg,data);

d = which('ft_defaults.m');
[pathstr, name, ext] = fileparts(d);
layoutPath = fullfile(pathstr,'template','layout');
layout = '64EEG'
switch layout
    case '64EEG'
        cfg = [];
        cfg.layout =fullfile(layoutPath,'EEG1010.lay');
        this.helperVariables.layout = ft_prepare_layout(cfg);
end

cfg = [];

%cfg.zlim = zlim;
%cfg.colormap = colormap('jet');
%cfg.maskstyle    = 'saturation';
%cfg.showlabels   = 'yes';
            cfg.baseline     ='yes';
cfg.layout       =  this.helperVariables.layout;
cfg.colorbar  ='yes';
cfg.showlabels = 'no';
cfg.showoutline ='yes';
figureH=  figure;
ft_multiplotER(cfg, data);
