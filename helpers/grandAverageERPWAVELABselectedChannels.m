maximumas1=[];
grandAverageKaire = [];
grandAverageCentras = [];
grandAverageDesine = [];
for imainList = 1:numel(mainList)
DT = [];
fileList = mainList{imainList};
for ifile = 1:numel(fileList)
    
    
    [path,name,ext] = fileparts(fileList{ifile});
    bb = erpwavelab_bt(fileList{ifile});
    data = bb.convertErpwavelabToFieldtripTimeFreq();
    cfg= [];
    channelsKaire = {'F1', 'F3', 'FC1', 'FC3', 'C1', 'C3'};
    cfg.channel = ft_channelselection(channelsKaire,data.label);
    cfg.avgoverchan = 'yes';
    cfg.frequency  = [10 80];
    cfg.avgoverfreq = 'no';
  %  cfg.latency = [-0.1, 0.6];
  %  cfg.avgovertime = 'no';
    [dataKaire] = ft_selectdata(cfg,data);
    channelsCentras = {'Fz','FCz','Cz'};
    cfg.channel = ft_channelselection(channelsCentras,data.label);
    [dataCentras] = ft_selectdata(cfg,data);
    channelsDesine = {'F2','F4','FC2','FC4', 'C2', 'C4'};
    cfg.channel = ft_channelselection(channelsDesine,data.label);
    [dataDesine] = ft_selectdata(cfg,data);
    
    DTkaire{ifile} = dataKaire;
    DTcentras{ifile} = dataCentras;
    DTdesine{ifile} = dataDesine;
end

grandAverageKaire{imainList} = ft_freqgrandaverage([],DTkaire{:});
grandAverageCentras{imainList} = ft_freqgrandaverage([],DTcentras{:});
grandAverageDesine{imainList} = ft_freqgrandaverage([],DTdesine{:});

end

for igrand= 1:numel(grandAverageKaire)
maximumas1(igrand) = max(max(max(grandAverageKaire{igrand}.powspctrm)));
end
maximumasKaire = max(maximumas1);


for igrand= 1:numel(grandAverageCentras)
maximumas1(igrand) = max(max(max(grandAverageCentras{igrand}.powspctrm)));
end
maximumasCentras = max(maximumas1);


for igrand= 1:numel(grandAverageDesine)
maximumas1(igrand) = max(max(max(grandAverageDesine{igrand}.powspctrm)));
end
maximumasDesine = max(maximumas1);

maximumas = [maximumasKaire, maximumasCentras,maximumasDesine];


for igrand = 1:numel(grandAverageKaire)
    cfg = [];
    cfg.colorbar  ='yes';
    cfg.showlabels = 'no';
    cfg.showoutline ='yes';
    cfg.interactive = 'no';
    cfg.linewidth     = 1;
    cfg.zlim = [0 maximumas];
    cfg.ylim = [30 50];
    cfg.xlim = [-100,600];
    figureH=  figure;
    ft_singleplotTFR(cfg, grandAverageKaire{igrand});
    [path,name,ext]=fileparts(mainList{igrand}{1});
    saveDir = fullfile(path,'grandAverageKaire');
    if ~isdir(saveDir)
        mkdir(saveDir);
    end
    filename = fullfile(saveDir,[name '_avg']);
    savefig(figureH,filename);
    print(figureH, filename, '-dpng');
    close(figureH);
    grandAvg = grandAverageKaire{igrand};
    save(fullfile(saveDir,'grandAverageFTKaire'),'grandAvg');
end



for igrand = 1:numel(grandAverageKaire)
    cfg = [];
    cfg.colorbar  ='yes';
    cfg.showlabels = 'no';
    cfg.showoutline ='yes';
    cfg.interactive = 'no';
    cfg.linewidth     = 1;
    cfg.zlim = [0 maximumas];
    cfg.ylim = [30 50];
    cfg.xlim = [-100,600];
    figureH=  figure;
    ft_singleplotTFR(cfg, grandAverageCentras{igrand});
    [path,name,ext]=fileparts(mainList{igrand}{1});
    saveDir = fullfile(path,'grandAverageCentras');
    if ~isdir(saveDir)
        mkdir(saveDir);
    end
    filename = fullfile(saveDir,[name '_avg']);
    savefig(figureH,filename);
    print(figureH, filename, '-dpng');
    close(figureH);
    grandAvg = grandAverageKaire{igrand};
    save(fullfile(saveDir,'grandAverageFTCentras'),'grandAvg');
end



for igrand = 1:numel(grandAverageDesine)
    cfg = [];
    cfg.colorbar  ='yes';
    cfg.showlabels = 'no';
    cfg.showoutline ='yes';
    cfg.interactive = 'no';
    cfg.linewidth     = 1;
    cfg.zlim = [0 maximumas];
    cfg.ylim = [30 50];
    cfg.xlim = [-100,600];
    figureH=  figure;
    ft_singleplotTFR(cfg, grandAverageDesine{igrand});
    [path,name,ext]=fileparts(mainList{igrand}{1});
    saveDir = fullfile(path,'grandAverageDesine');
    if ~isdir(saveDir)
        mkdir(saveDir);
    end
    filename = fullfile(saveDir,[name '_avg']);
    savefig(figureH,filename);
    print(figureH, filename, '-dpng');
    close(figureH);
    grandAvg = grandAverageKaire{igrand};
    save(fullfile(saveDir,'grandAverageFTDesine'),'grandAvg');
end