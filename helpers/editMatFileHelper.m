for ii=1:numel(fileList)
    
    
    m = matfile(fileList{ii},'Writable',true);
    sz=size(m.chanlocs,2);
    C = load(fileList{ii}, 'chanlocs');
    
   
    C.chanlocs(1:12) = [];
    C.chanlocs(end-1:end) = [];
    m.chanlocs = C.chanlocs;
    C = load(fileList{ii}, 'WT');
    C.WT(1:12,:,:) = [];
    C.WT(end-1:end,:,:) = [];
     m.WT = C.WT;
end