for iFileList =1:numel(fileList)
    load(fileList{iFileList})
    [path, nameExcel, ext] = fileparts(fileList{iFileList});
    writer = bt_excell_csvExport('xls', nameExcel, path);
    
    for ichan =1:numel(data.calculation(1).chanlocs)
            c=cellstr('time');
            n=num2cell(data.calculation(1).tim(:));
            time=[c; n];
            excell = time;
        for iFile = 1:numel(data.calculation)
            [path, name, ext] =  fileparts(data.matFileList{iFile});
            dataRow = squeeze(data.calculation(iFile).WT(ichan,:,:));
            channel = data.calculation(iFile).chanlocs(ichan).labels;
        

            c=cellstr(name);
            n=num2cell(dataRow);
            dataR=[c; n];
            excell = [excell, dataR];
        end
            writer.exportToXls(excell,ichan,channel,'A1') 
    end
    
    
    
    
end%ifilelist

