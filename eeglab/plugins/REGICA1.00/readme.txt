% Automatic EOG Artifact Rejection using REGICA (REGICA v1.0) 1.0 plug-in for EEGLAB, Release 26-01-2011 
%
% Author: Manousos Klados 
%         Group of Applied Neurosciences
%         Lab of Medical Informatics
%         Medical School
%         Aristotle University of Thessaloniki
%         P.O. Box 323, 54124 Thessaloniki
%         Greece
% 
%         e-mail: mklados (at) med.auth.gr
%         �el: +30-2310-999332
%         Fax: +30-2310-999263
%         Personal Page: http://lomiweb.med.auth.gr/gan/mklados/ 
%
% Installation instructions:
% 1) Install EEGLAB toolbox for Matlab (http://www.sccn.ucsd.edu/eeglab/). 
% 2) In the plugins folder of the EEGLAB installation folder create a folder
%    called REGICA and place all the files of the REGICAv1.0 plug-in inside that 
%    folder.
% 3) Add the REGICA folder that you created in the previous step to the
%    Matlab path
% 4) Run EEGLAB. Under the Tools menu there should be a sub-menu named 
%    "Automatic EOG Artifact Rejection using REGICA" which contains the REGICA functions.
% 5) [optional] Install, i.e. include in Matlab path, the following Independent
%    Component Analysis (ICA) algorithms:
%    5.1) JADE: 
%         (c) Jean-Francois Cardoso
%         WWW: http://www.tsi.enst.fr/~cardoso/Algo/Jade/jadeR.m
%    5.2) FastICA:
%	  (c) H. G�vert, J. Hurri, J. S�rel� and A. Hyv�rinen
%	  WWW: http://www.cis.hut.fi/projects/ica/fastica/
%
% Note: If you installed any previous version of the toolbox you should 
% remove it from the Matlab path before installing the new version
%
%
% Version history:
% Release 26-01-2011 version 1.0 Original Release

% LICENSE INFORMATION:
% Copyright (C) <2011>  <Manousos Klados>
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
%
%
%
% Aknowledgments
% This software package includes parts from the EEGLAB toolbox, as well as 
% from the Automatic Artifact Removal v 1.3 (AAR1.3) plugin for EEGLAB developed 
% by German Gomez-Herrero. 
% 
% Bug Reporting
% All the bug reportings are welcome. Feel free to send them to the author's e-mailing 
% address (mklados@med.auth.gr) and in the next version of the REGICA plugin they will 
% be addressed. 
% 



   

 
