function bt_install()
d = which('bt_install');
[pathstr, name, ext] = fileparts(d);

currentPath = matlabpath;
restoredefaultpath;
addpath(genpath(pathstr))
currentDir = pwd;

% check git
[status,~] = system('git --version');
if status == 1
    
    web('http://git-scm.com/download', '-browser')
    error('Install git: http://git-scm.com/download')
    cd(currentDir)
    path(currentPath)
end

if status == 0
    if exist('addPaths','file') == 2
        d = which('addPaths');
        [pathstr, name, ext] = fileparts(d);
        cd(pathstr);
        try
            disp('UPDATE')
            system('git config merge.renameLimit 999999');
            system('git fetch --all');
            system('git reset --hard origin/master');
            system('git config --unset merge.renameLimit');
            system('git pull');
            
            
        catch
            error('Failed to update')
            cd(currentDir)
            path(currentPath)
        end
    else
        system('git clone https://aaleks@bitbucket.org/aaleks/compileddatabatch.git');       
    end
    
end
% restore previuos settings
cd(currentDir)
path(currentPath)
end